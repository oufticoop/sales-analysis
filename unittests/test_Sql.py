import unittest
import psycopg2

from src import Sql

url="localhost"
port = 5432
user = "postgres"
password = "pwd"

class TestSqlConnect(unittest.TestCase):

    def test_connect_with_valid_infos(self):

        sql = Sql.Sql("localhost", 5432, "postgres", "pwd")
        self.assertIsInstance(sql, Sql.Sql)

    def test_connect_invalid_url_wrong_domaine_name(self):
        
        self.assertRaises(ConnectionError, Sql.Sql, "foo_bar", port, user, password)

    def test_connect_invalid_port(self):
        
        self.assertRaises(ConnectionError, Sql.Sql, url, 1234, user, password)

    def test_connect_invalid_username(self):
        self.assertRaises(ConnectionError, Sql.Sql, url, port, "foo", password)

    def test_connect_invalid_password(self):
        self.assertRaises(ConnectionError, Sql.Sql, url, port, user, "bar")




class TestSqlCreateTable(unittest.TestCase):

    def setUp(self):
        # Connect to DB
        self._sql = Sql.Sql(url, port, user, password)

        # Drop all tables
        self._sql.execute_raw_sql("""
            DROP SCHEMA public CASCADE;
            CREATE SCHEMA public;
        """)

    def tearDown(self):
        # Disconnect to DB
        self._sql.disconnect()

    def test_new_table(self):
        self._sql.create_table("foo.bar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col1': 'VARCHAR(100)',
            'col2': 'TEXT'
        })

        cursor = self._sql.execute_raw_sql("SELECT column_name,data_type FROM information_schema.columns where table_name='foo.bar'")
        self.assertEqual(cursor.fetchall(), [
            ('id', 'integer'),
            ('col1', 'character varying'),
            ('col2', 'text')
        ])

    def test_existing_table_same_fields(self):
        new_columns = self._sql.create_table("foobar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col2': 'TEXT'
        })

        self.assertEqual(new_columns, [])
        
        new_columns = self._sql.create_table("foobar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col2': 'TEXT'
        })

        self.assertEqual(new_columns, [])

        cursor = self._sql.execute_raw_sql("SELECT column_name,data_type FROM information_schema.columns where table_name='foobar'")
        self.assertEqual(cursor.fetchall(), [
            ('id', 'integer'),
            ('col2', 'text')
        ])

    def test_existing_table_more_fields(self):
        self._sql.create_table("foo.bar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col2': 'TEXT'
        })

        self._sql.populate("foo.bar", 'id', [{'id': 1, 'col2': 'lorem'}, {'id': 2, 'col2': 'ipsum'}])

        new_columns = self._sql.create_table("foo.bar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col1': 'TEXT',
            'col2': 'TEXT'
        })

        self.assertEqual(new_columns, ['col1'])

        cursor = self._sql.execute_raw_sql("SELECT column_name,data_type FROM information_schema.columns where table_name='foo.bar' ORDER BY column_name")
        self.assertEqual(cursor.fetchall(), [
            ('col1', 'text'),
            ('col2', 'text'),
            ('id', 'integer')
        ])

        cursor = self._sql.execute_raw_sql('SELECT * FROM "foo.bar"')
        self.assertEqual(cursor.fetchall(), [
            (1, 'lorem', None),
            (2, 'ipsum', None),
        ])




    def test_existing_table_less_fields(self):
        pass

    def test_new_table_invalid_structure(self):
        self.assertRaises(psycopg2.errors.UndefinedObject, self._sql.create_table, "foobar", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'col1': 'foo',
        })


class TestSqlPopulateTable(unittest.TestCase):

    def setUp(self):
        # Connect to DB
        self._sql = Sql.Sql(url, port, user, password)

        # Drop all tables
        self._sql.execute_raw_sql("""
            DROP SCHEMA public CASCADE;
            CREATE SCHEMA public;
        """)

        self._sql.create_table("test.table", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'foo': 'VARCHAR(100)',
            'bar': 'int'
        })

    def test_populate_with_data_for_all_fields(self):

        self._sql.populate('test.table', 'id', [
            {'id': 1, 'foo': "ROCK AND ROLL !"},
            {'id': 2, 'foo': "jazz"}
        ])

        cursor = self._sql.execute_raw_sql('SELECT id,foo FROM "test.table"')
        self.assertEqual(cursor.fetchall(), [
            (1, 'ROCK AND ROLL !'),
            (2, 'jazz'),
        ])

    def test_populate_with_data_for_one_field(self):
        pass


    def test_populate_with_no_data(self):
        pass

    def test_populate_with_existing_data_not_changed(self):
        self._sql.populate('test.table', 'id', [
            {'id': 1, 'foo': "ROCK AND ROLL !"},
            {'id': 2, 'foo': "jazz"}
        ])

        self._sql.populate('test.table', 'id', [
            {'id': 1, 'foo': "ROCK AND ROLL !"},
        ])        
        cursor = self._sql.execute_raw_sql('SELECT id,foo FROM "test.table" ORDER BY id')
        self.assertEqual(cursor.fetchall(), [
            (1, 'ROCK AND ROLL !'),
            (2, 'jazz'),
        ])

    def test_populate_with_existing_data_changed(self):
        self._sql.populate('test.table', 'id', [
            {'id': 1, 'foo': "ROCK AND ROLL !"},
            {'id': 2, 'foo': "jazz"}
        ])

        self._sql.populate('test.table', 'id', [
            {'id': 1, 'foo': "blues"},
        ])        
        cursor = self._sql.execute_raw_sql('SELECT id,foo FROM "test.table" ORDER BY id')
        self.assertEqual(cursor.fetchall(), [
            (1, 'blues'),
            (2, 'jazz'),
        ])


    def test_populate_with_data_wrong_type(self):
        pass

class TestSqlGetLastValue(unittest.TestCase):

    def setUp(self):
        # Connect to DB
        self._sql = Sql.Sql(url, port, user, password)

        # Drop all tables
        self._sql.execute_raw_sql("""
            DROP SCHEMA public CASCADE;
            CREATE SCHEMA public;
        """)

        self._sql.create_table("test.table", {
            'id': 'INT PRIMARY KEY NOT NULL',
            'date': 'TIMESTAMP'
        })

    def test_get_last_value_no_data(self):
        self.assertEqual(self._sql.get_last_value('test.table', 'date'), None)

    def test_get_last_value_one_data(self):
        self._sql.populate("test.table", [{'id': 1, 'date': '2022-04-20'}])
        self.assertEqual(self._sql.get_last_value('test.table', 'date'), '2022-04-20 00:00:00.000000')

    def test_get_last_value_many_data(self):
        self._sql.populate("test.table", [
            {'id': 1, 'date': '2022-04-20'},
            {'id': 2, 'date': '2021-04-20'},
            {'id': 3, 'date': '2023-04-20'},
            {'id': 4, 'date': '2020-04-20'}
        ])
        self.assertEqual(self._sql.get_last_value('test.table', 'date'), '2023-04-20 00:00:00.000000')

    def test_get_last_value_no_table(self):
        pass

