
from datetime import date

class LinesInPeriods:
    
    def __init__(self, lines, start, stop, tfield_name):
        self._lines = lines
        self._start = start
        self._stop = stop
        self._tfield_name = tfield_name

    def __iter__(self):
        self._lines_iter = iter(self._lines)
        return self

    def __next__(self):
        # Find first element that match the period:
        for line in self._lines_iter:
            if date.fromisoformat(line[self._tfield_name][:10]) < date.fromisoformat(self._start):
                continue
            elif date.fromisoformat(line[self._tfield_name][:10]) >= date.fromisoformat(self._stop):
                raise StopIteration

            return line

        raise StopIteration
