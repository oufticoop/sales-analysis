
from typing import Type
from datetime import date, datetime

from src.Client import Client
from src.Products import Products
import src.utils as utils


class Sales():

    def __init__(self, periods):
        
        # Get all order lines from first day of first period to last day of last period
        self._pos_lines = Client().get('pos.order.line', [
            ['create_date', '>', periods[0]],
            ['create_date', '<', periods[-1]]
        ], fields=['product_id', 'price_subtotal', 'qty', "create_date"], order_by="create_date")

        self._sale_lines = Client().get('sale.order.line', [
            ['create_date', '>', periods[0]],
            ['create_date', '<', periods[-1]]
        ], fields=['product_id', 'price_subtotal', 'qty_delivered', "create_date"], order_by="create_date")

    def total_per_product(self, period_start, period_end):

        sums = {}

        def loop_over(lines):
            for line in utils.LinesInPeriods(lines, period_start, period_end, "create_date"):

                try:
                    product_id = line['product_id'][0]
                except TypeError:
                    print("wierd behaviour...")
                else:
                    if product_id in sums:
                        sums[product_id] += line['price_subtotal']
                    else:
                        sums[product_id] = line['price_subtotal']

        loop_over(self._pos_lines)
        loop_over(self._sale_lines)

        return sums

    def total_per_category(self, start_time, end_time):
        
        sums = {}

        for line in self._get_all_sales(start_time, end_time):
            # Check if there is products in the sale, otherwise continue to next line
            if line['product_id']:
                product_id = line['product_id'][0]
            else:
                continue

            try:
                cat = Products().products[product_id]['cat']
            except KeyError:
                print(f"Unable to find product {product_id}")
            if cat in sums:
                sums[cat] += line['price_subtotal']
            else:
                sums[cat] = line['price_subtotal']

        return sums

    def _get_all_sales(self, start_time, end_time):

        lines = Client().get('pos.order.line', [
            ['create_date', '>', start_time],
            ['create_date', '<', end_time]
        ], fields=['product_id', 'price_subtotal', 'qty'])

        lines += Client().get('sale.order.line', [
            ['create_date', '>', start_time],
            ['create_date', '<', end_time]
        ], fields=['product_id', 'price_subtotal', 'qty_delivered'])

        return lines


    def get_mean_sales_per_day_of_week(self, start_time, end_time):

        ca = [0] * 7

        for line in utils.LinesInPeriods(self._pos_lines, start_time, end_time, "create_date"):
            ca[datetime.fromisoformat(line['create_date']).weekday()] += line['price_subtotal']

        return ca