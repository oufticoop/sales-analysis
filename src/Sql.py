import psycopg2

"""
Handle SQL connection, creating tables, read, write, etc
"""

sql = None

class SqlClass:

    _cursor = None

    def __init__(self, url, port, username, password, dbname="postgres"):
        """
        Connect
        """

        self.connect(url, port, username, password, dbname)

    def connect(self, url, port, username, password, dbname):
        """
        Handle the connection to the DB server (or initialize if it's SQLite)
        """
        try:
            self._connection = psycopg2.connect(f"host={url} port={port} user={username} password={password} dbname={dbname}")
        except psycopg2.OperationalError:
            raise ConnectionError
        else:
            self._cursor = self._connection.cursor()
    
    def disconnect(self):
        pass
        # print("doing this")
        # self._cursor.close()
        # del self._cursor

    def execute_raw_sql(self, resquest, commit=False):
        self._cursor.execute(resquest)
        if commit:
            self._connection.commit()
        return self._cursor

    def create_table(self, name, columns):
        """
        Create a new table
        Update table structure if table already exists (only by adding columns)
        """
        try:
            self.execute_raw_sql(f"""
                CREATE TABLE "{name}" (
                    {','.join(f'{key} {value}' for key,value in columns.items())}
                );
            """)
            new_columns = []
        except psycopg2.errors.DuplicateTable:
            self._connection.rollback()
            existing_columns = [
                i[0] for i in self.execute_raw_sql(f"SELECT column_name FROM information_schema.columns WHERE table_name = '{name}';").fetchall()
            ]
            new_columns = list(set(columns) - set(existing_columns))

            for column in new_columns:
                self.execute_raw_sql(f"""
                    ALTER TABLE "{name}" ADD COLUMN {column} {columns[column]}
                """)

        self._connection.commit()

        return new_columns

    def populate(self, table, prim, inputs):


        # To be replaced by  psycopg2.extras.execute_batch (better performances)
        inputs = [{
            key: "'" + value.replace("'", "''") + "'" if type(value) == str  and value != "NULL" else f"{value}" for key,value in input.items()
        } for input in inputs]

        coted_table = f'"{table}"'
        operation = f"""
        INSERT INTO {coted_table} ({','.join(inputs[0].keys())}) VALUES {','.join([f"({','.join(input.values())})" for input in inputs])}
        """
        if prim:
            operation += f"""
            ON CONFLICT ({prim}) DO 
            UPDATE SET {','.join([f"{key}=EXCLUDED.{key}" for key in  inputs[0].keys()])}
            """
        operation += ";"
        
        # print(f"Executing {operation[:200]}... {operation[len(operation) - 200:]}")
        self._cursor.execute(operation)


        # for input in inputs:
        #     corrected_input = {
        #         key: "'" + value.replace("'", "''") + "'" if type(value) == str else f"{value}" for key,value in input.items()
        #     }
        #     coted_table = f'"{table}"'
        #     operation = f"""
        #     INSERT INTO {coted_table} ({','.join(corrected_input.keys())}) VALUES ({','.join(corrected_input.values())})
        #     """
        #     if prim:
        #         operation += f"""
        #         ON CONFLICT ({prim}) DO 
        #         UPDATE SET {','.join([f'{key} = {value}' for key,value in corrected_input.items()])}
        #         """
        #     operation += ";"
        #     self._cursor.execute(operation)

        #     print(input)
        
        self._connection.commit()

    def get_last_value(self, table, time_field):
        # Retrieve the date and time of the last data recorded in a specific table
        data = self.execute_raw_sql(f"""
             SELECT {time_field} FROM "{table}" WHERE {time_field} IS NOT NULL ORDER BY {time_field} DESC LIMIT 1;
        """).fetchone()

        if data:
            return data[0].strftime("%Y-%m-%d %H:%M:%S")
        else:
            return None
    

def Sql(*args):
    global sql
    if not sql:
        sql = SqlClass(*args)
    
    return sql