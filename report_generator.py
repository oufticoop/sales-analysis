
"""
Responsible to copy the Odoo Database into a Postgres server based on the configuration inside the database_copy.yml file
"""
import re

import argparse
import yaml
import datetime

from src.Sql import Sql
from src.Client import Client

from report_functions import *

def compute_data(sql, match):

    print(match)

    exec(f"{match}(sql)")

    return f"{match}.png"
    
    
def generate_graphs(sql, match):
    print(match)
    function_name = match.group(1)

    exec(f"{function_name}(sql)")

    return f"{function_name}.pdf"



def generate(sql, template):
    # Create and populate analysis tables:
    result = re.sub(r'\{\{([^\{\}]*?)\}\}', lambda x: generate_graphs(sql, x), template, count=0, flags=(re.MULTILINE | re.DOTALL))

    with open("reports/report.tex", 'w') as fd:
        fd.write(result) 


if __name__ == '__main__':

    # parser = argparse.ArgumentParser()
    # parser.add_argument("--analysis", "-a", help="Specify an analysis to perform. Otherwise, perform all", nargs='+')
    # args = parser.parse_args()

    with open("cfg/credentials.yml") as fd:
        credentials = yaml.load(fd.read())

    # read config file
    with open('cfg/report_templates/report.tex', 'r') as fd:
        template = fd.read()

    sql = Sql(
        credentials['database']['host'],
        credentials['database']['port'],
        credentials['database']['user'],
        credentials['database']['pwd']
    )
        
    generate(sql, template)
